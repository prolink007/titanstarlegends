import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./components/home/home.component";
import {NewsArticleComponent} from "./components/news-article/news-article.component";

const routes: Routes = [
    {path: "", component: HomeComponent},
    {path: "news/:id", component: NewsArticleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
