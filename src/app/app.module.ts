import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";

import {AppComponent} from "./app.component";
import { HomeComponent } from './components/home/home.component';
import { SocialMediaComponent } from './components/social-media/social-media.component';
import { SearchComponent } from './components/search/search.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { NewsPreviewComponent } from './components/news-preview/news-preview.component';
import { TalentCalculatorComponent } from './components/talent-calculator/talent-calculator.component';
import { TalentPathComponent } from './components/talent-path/talent-path.component';
import { TalentNodeComponent } from './components/talent-node/talent-node.component';
import { NewsArticleComponent } from './components/news-article/news-article.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SocialMediaComponent,
    SearchComponent,
    NavigationComponent,
    FooterComponent,
    NewsPreviewComponent,
    TalentCalculatorComponent,
    TalentPathComponent,
    TalentNodeComponent,
    NewsArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
