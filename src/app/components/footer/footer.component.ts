import {Component} from "@angular/core";
import {NavigationService} from "../../services/navigation.service";
import {INavigationService} from "../../services/INavigationService";
import {INavButtonModel} from "../../models/INavButtonModel";

/**
 * The footer for the web app.
 */
@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.scss"],
    providers: [NavigationService]
})
export class FooterComponent {

    /**
     * A dumb service just for having navigation in multiple places.
     */
    private _navService: INavigationService;

    constructor(navService: NavigationService) {
        this._navService = navService;
    }

    /**
     * @returns {INavButtonModel[]} The navigation options for the webapp.
     */
    public get navOptions(): INavButtonModel[] {
        return this._navService.navButtons;
    }
}
