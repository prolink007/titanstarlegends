import {Component} from "@angular/core";

/**
 * The home page for the web app.
 */
@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent {

}
