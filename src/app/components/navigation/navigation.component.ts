import {Component} from "@angular/core";
import {INavButtonModel} from "../../models/INavButtonModel";
import {NavigationService} from "../../services/navigation.service";
import {INavigationService} from "../../services/INavigationService";

/**
 * The navigation component on the header bar.
 */
@Component({
    selector: "app-navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"],
    providers: [NavigationService]
})
export class NavigationComponent {

    /**
     * A dumb service just for having navigation in multiple places.
     */
    private _navService: INavigationService;

    constructor(navService: NavigationService) {
        this._navService = navService;
    }

    /**
     * @returns {INavButtonModel[]} The navigation buttons to show.
     */
    public get navButtons(): INavButtonModel[] {
        return this._navService.navButtons;
    }
}
