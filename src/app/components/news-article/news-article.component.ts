import {Component, OnInit} from "@angular/core";
import {INewsArticle} from "../../models/INewsArticle";
import {NewsService} from "../../services/news.service";
import {INewsService} from "../../services/INewsService";
import {ParamMap, ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/switchMap";

/**
 * A component for showing a news article.
 */
@Component({
    selector: "app-news-article",
    templateUrl: "./news-article.component.html",
    styleUrls: ["./news-article.component.scss", "../news-preview/news-preview.component.scss"],
    providers: [NewsService]
})
export class NewsArticleComponent implements OnInit {

    /**
     * The service for getting news articles.
     */
    private _newsService: INewsService;

    /**
     * The current news article to show.
     */
    private _newsArticle: INewsArticle;

    private _route: ActivatedRoute;

    constructor(newsService: NewsService, route: ActivatedRoute) {
        this._newsService = newsService;
        this._route = route;
    }

    public ngOnInit(): void {
        this._route.paramMap.switchMap(
            (params: ParamMap) => {
                // get the id off the route
                const id: number = parseInt(params.get("id"), 10);
                // map that to the service getting the article
                return this._newsService.getNewsById(id);
            }
        ).subscribe(
            (article: INewsArticle) => {
                // set the article
                this._newsArticle = article;
            }
        );
    }

    public get title(): string {
        return (this._newsArticle) ? this._newsArticle.title : "";
    }

    public get author(): string {
        return (this._newsArticle) ? this._newsArticle.author : "";
    }

    public get body(): string {
        return (this._newsArticle) ? this._newsArticle.body : "";
    }

    public get time(): Date {
        return (this._newsArticle) ? this._newsArticle.time : new Date();
    }
}
