import {Component, OnInit} from "@angular/core";
import {NewsService} from "../../services/news.service";
import {INewsService} from "../../services/INewsService";
import {INewsArticle} from "../../models/INewsArticle";
import {Router} from "@angular/router";

/**
 * Shows a preview of an article.
 */
@Component({
    selector: "app-news-preview",
    templateUrl: "./news-preview.component.html",
    styleUrls: ["./news-preview.component.scss"],
    providers: [NewsService]
})
export class NewsPreviewComponent implements OnInit {

    /**
     * The service for getting news articles.
     */
    private _newsService: INewsService;

    /**
     * The current news article to show.
     */
    private _newsArticle: INewsArticle;
    private _router: Router;

    constructor(newsService: NewsService, router: Router) {
        this._newsService = newsService;
        this._router = router;
    }

    public ngOnInit() {

        // Generating a random number between 1 and the number of articles i have mocked. To show a random
        // article every time it loads.
        const randomIndex: number = Math.floor(Math.random() * 5) + 1;

        this._newsService.getNewsById(randomIndex).subscribe(
            (newsArticle: INewsArticle) => {
                this._newsArticle = newsArticle;
            }
        );
    }

    /**
     * Called when the read more button is clicked.
     */
    public onReadMoreClicked(): void {
        this._router.navigate(["/news", 1]);
    }

    public get title(): string {
        return (this._newsArticle) ? this._newsArticle.title : "";
    }

    public get author(): string {
        return (this._newsArticle) ? this._newsArticle.author : "";
    }

    public get body(): string {
        return (this._newsArticle) ? this._newsArticle.body : "";
    }

    public get time(): Date {
        return (this._newsArticle) ? this._newsArticle.time : new Date();
    }
}
