import {Component} from "@angular/core";

/**
 * A component for handling search.
 *
 * Does nothing. Just looks good.
 */
@Component({
    selector: "app-search",
    templateUrl: "./search.component.html",
    styleUrls: ["./search.component.scss"]
})
export class SearchComponent {
}
