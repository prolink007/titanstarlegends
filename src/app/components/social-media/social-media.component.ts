import {Component} from "@angular/core";

/**
 * Holds social media buttons.
 *
 * Can easily be made to handle a dynamic number of social media.
 */
@Component({
    selector: "app-social-media",
    templateUrl: "./social-media.component.html",
    styleUrls: ["./social-media.component.scss"]
})
export class SocialMediaComponent {
}
