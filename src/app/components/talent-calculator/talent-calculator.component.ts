import {Component, OnDestroy, OnInit} from "@angular/core";
import {TalentService} from "../../services/talent.service";
import {ITalentService} from "../../services/ITalentService";
import {ITalentTree} from "../../models/ITalentTree";
import {ITalentPath} from "../../models/ITalentPath";
import {TalentCommsService} from "../../services/talent-comms.service";
import {ITalentCommsService} from "../../services/ITalentCommsService";
import {Subscription} from "rxjs/Subscription";
import {Observer} from "rxjs/Observer";
import {ITalentNode} from "../../models/ITalentNode";

/**
 * A component for displaying talent calculation.
 */
@Component({
    selector: "app-talent-calculator",
    templateUrl: "./talent-calculator.component.html",
    styleUrls: ["./talent-calculator.component.scss"],
    providers: [TalentService, TalentCommsService]
})
export class TalentCalculatorComponent implements OnInit, OnDestroy {

    /**
     * A service for getting the available talents.
     */
    private _talentService: ITalentService;

    /**
     * A service for handling communications between this component and anyone else who wants to listen.
     */
    private _talentCommsService: ITalentCommsService;

    /**
     * The current talent tree.
     */
    private _talentTree: ITalentTree;

    /**
     * A subscription to the talents comms.
     */
    private _talentCommsSubscription: Subscription;

    /**
     * The total points spent in the calculator.
     */
    private _totalPointsSpent: number;

    constructor(talentService: TalentService, talentCommsService: TalentCommsService) {
        this._totalPointsSpent = 0;
        this._talentService = talentService;
        this._talentCommsService = talentCommsService;
        this._talentCommsSubscription =
            this._talentCommsService.talentSelected().subscribe(this.talentSelectedObserver());
    }

    public ngOnInit() {
        // Getting the available talents.
        this._talentService.getRuneMasteryTalentTree().subscribe(
            (talentTree: ITalentTree) => {
                this._talentTree = talentTree;
            }
        );
    }

    public ngOnDestroy(): void {
        // cleaning up the subscription
        if (this._talentCommsSubscription) {
            this._talentCommsSubscription.unsubscribe();
        }
    }

    public get talentTreeName(): string {
        return (this._talentTree) ? this._talentTree.name : "";
    }

    public get totalPointsSpent(): number {
        return this._totalPointsSpent;
    }

    public get totalPointsAvailable(): number {
        return (this._talentTree) ? this._talentTree.totalPointsAvailable : 0;
    }

    public get talentPaths(): ITalentPath[] {
        return (this._talentTree) ? this._talentTree.talentPaths : [];
    }

    /**
     * An Observer for when a talent is selected.
     */
    private talentSelectedObserver(): Observer<ITalentNode> {
        return {
            next: (currentTalentNode: ITalentNode) => {
                // Check if currentnode is valid for modification
                if (currentTalentNode && currentTalentNode.valid) {
                    // check if the currentnode is already selected
                    if (currentTalentNode.selected) {
                        // invalidate the current node and the rest in the path
                        this.invalidateCurrentAndNext(currentTalentNode);
                    } else if (this._talentTree &&
                        (this._totalPointsSpent < this._talentTree.totalPointsAvailable)) {
                        // check if the total number of talent points have been met or exceeded.

                        // this node is valid and can be selected, increment the points spent and toggle selection
                        this._totalPointsSpent++;
                        currentTalentNode.selected = true;
                        // check if currentnode has a next and the next is invalid (not selectable)
                        if (currentTalentNode.next && !currentTalentNode.next.valid) {
                            // set the next to valid, so it can be checked.
                            currentTalentNode.next.valid = true;
                        }
                    }
                }
            },
            error: () => {},
            complete: () => {}
        };
    }

    /**
     * Recursively invalidates the current node and next nodes.
     *
     * @param {ITalentNode} currentTalentNode The current node to start invalidating.
     */
    private invalidateCurrentAndNext(currentTalentNode: ITalentNode): void {
        // check if the current node is selected.
        if (currentTalentNode.selected) {
            // decrement the points spent because one was just unselected.
            this._totalPointsSpent--;
        }

        // the node was just unselected
        currentTalentNode.selected = false;
        // check if the currentnode has a next, otherwise leave.
        if (currentTalentNode.next) {
            currentTalentNode.next.valid = false;
            // invalidate the next node of the current node. o.O go nuts
            return this.invalidateCurrentAndNext(currentTalentNode.next);
        }
    }
}
