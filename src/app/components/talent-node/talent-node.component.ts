import {Component, Input} from "@angular/core";
import {ITalentNode} from "../../models/ITalentNode";
import {TalentCommsService} from "../../services/talent-comms.service";
import {ITalentCommsService} from "../../services/ITalentCommsService";

/**
 * A component for displaying a talent node.
 */
@Component({
    selector: "app-talent-node",
    templateUrl: "./talent-node.component.html",
    styleUrls: ["./talent-node.component.scss"]
})
export class TalentNodeComponent {

    /**
     * The talent node to show.
     */
    private _talentNode: ITalentNode;

    /**
     * A service for handling communications between this component and anyone else who wants to listen.
     */
    private _talentCommsService: ITalentCommsService;

    constructor(talentCommsService: TalentCommsService) {
        this._talentCommsService = talentCommsService;
    }

    /**
     * Called when the node is selected.
     */
    public onSelected(): void {
        if (this._talentNode && this._talentNode.valid) {
            // Tell the comms service the node was selected.
            this._talentCommsService.selectTalent(this._talentNode);
        }
    }

    @Input()
    public set talentNode(talentNode: ITalentNode) {
        this._talentNode = talentNode;
    }

    public get name(): string {
        return (this._talentNode) ? this._talentNode.name : "";
    }

    public get pointMax(): number {
        return (this._talentNode) ? this._talentNode.pointMax : 0;
    }

    public get selected(): boolean {
        return (this._talentNode) ? this._talentNode.selected : false;
    }

    public get icon(): string {
        if (this._talentNode) {
            if (this._talentNode.selected) {
                return this._talentNode.fullIcon;
            } else {
                return this._talentNode.notFullIcon;
            }
        } else {
            return "";
        }
    }
}
