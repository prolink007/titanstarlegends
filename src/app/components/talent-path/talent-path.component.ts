import {Component, Input} from "@angular/core";
import {ITalentPath} from "../../models/ITalentPath";
import {ITalentNode} from "../../models/ITalentNode";

/**
 * A component for holding the talent path.
 */
@Component({
    selector: "app-talent-path",
    templateUrl: "./talent-path.component.html",
    styleUrls: ["./talent-path.component.scss"]
})
export class TalentPathComponent {

    private _talentPath: ITalentPath;

    @Input()
    public set talentPath(talentPath: ITalentPath) {
        this._talentPath = talentPath;
    }

    public isLinkActive(talentNode: ITalentNode): boolean {
        if (talentNode && talentNode.next && talentNode.next.selected) {
            return true;
        } else {
            return false;
        }
    }

    public get name(): string {
        return (this._talentPath) ? this._talentPath.name : "";
    }

    public get talentNodes(): ITalentNode[] {
        return (this._talentPath) ? this._talentPath.talentNodes : [];
    }
}
