export interface INavButtonModel {
    displayName: string;
    imagePath: string;
    subNavigation?: string[];
}
