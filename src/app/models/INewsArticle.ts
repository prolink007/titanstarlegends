export interface INewsArticle {
    id: number;
    title: string;
    body: string;
    author: string;
    time: Date;
}
