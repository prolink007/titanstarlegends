export interface ITalentNode {
    name?: string;
    fullIcon: string;
    notFullIcon: string;
    pointMax: number;
    selected?: boolean;
    valid?: boolean;
    previous?: ITalentNode;
    next?: ITalentNode;
}
