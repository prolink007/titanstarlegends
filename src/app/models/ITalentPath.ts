import {ITalentNode} from "./ITalentNode";

export interface ITalentPath {
    talentNodes: ITalentNode[];
    name?: string;
}
