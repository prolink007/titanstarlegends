import {ITalentPath} from "./ITalentPath";

export interface ITalentTree {

    // I know it is a bit over kill to have these as an interface, but my brain fusses at me if i dont
    // try and future proof. I could just have a 2d array of nodes.
    /**
     *
     */
    talentPaths: ITalentPath[];
    name?: string;
    totalPointsAvailable: number;
}
