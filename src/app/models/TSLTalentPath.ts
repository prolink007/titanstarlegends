import {ITalentPath} from "./ITalentPath";
import {ITalentNode} from "./ITalentNode";

/**
 * Custom implementation of the ITalentPath to provide an adding method.
 */
export class TSLTalentPath implements ITalentPath {

    private _name: string;
    private _talentNodes: ITalentNode[];

    constructor() {
        this._talentNodes = [];
    }

    /**
     * Adds a talent node to the tree on this path.
     *
     * Setting the previous and next nodes for the added node.
     *
     * @param {ITalentNode} currentNode The talent node to add.
     */
    public addTalentNode(currentNode: ITalentNode): void {
        // if the node does not exist or is first.
        if (!this._talentNodes || this._talentNodes.length === 0) {
            this._talentNodes = [];
            currentNode.previous = null;
            currentNode.next = null;
            this._talentNodes.push(currentNode);
        } else {
            const previousNode: ITalentNode = this._talentNodes[this._talentNodes.length - 1];
            previousNode.next = currentNode;
            currentNode.previous = previousNode;
            currentNode.next = null;
            this._talentNodes.push(currentNode);
        }
    }

    public get talentNodes(): ITalentNode[] {
        return this._talentNodes;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get name(): string {
        return this._name;
    }
}
