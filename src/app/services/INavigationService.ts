import {INavButtonModel} from "../models/INavButtonModel";

/**
 * A service for providing navigation, since it was in multiple places, i did not want to dup code.
 */
export interface INavigationService {
    navButtons: INavButtonModel[];
}
