import {Observable} from "rxjs/Observable";
import {INewsArticle} from "../models/INewsArticle";

/**
 * A service for providing news articles.
 */
export interface INewsService {
    /**
     * Gets news by id.
     * @param {number} id The id of the article.
     * @returns {Observable<INewsArticle>} The article with the id.
     */
    getNewsById(id: number): Observable<INewsArticle>;
}
