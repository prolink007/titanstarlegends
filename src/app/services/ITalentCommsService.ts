import {ITalentNode} from "../models/ITalentNode";
import {Observable} from "rxjs/Observable";

/**
 * A service for handling communications between components and anyone else who wants to listen.
 */
export interface ITalentCommsService {
    talentSelected(): Observable<ITalentNode>;
    selectTalent(talentNode: ITalentNode): void;
}
