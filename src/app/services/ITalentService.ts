import {ITalentTree} from "../models/ITalentTree";
import {Observable} from "rxjs/Observable";

/**
 * A service for providing available talents.
 */
export interface ITalentService {
    getRuneMasteryTalentTree(): Observable<ITalentTree>;
}
