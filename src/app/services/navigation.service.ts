import {Injectable} from "@angular/core";
import {INavButtonModel} from "../models/INavButtonModel";
import {INavigationService} from "./INavigationService";

@Injectable()
export class NavigationService implements INavigationService {

    private _navButtons: INavButtonModel[];

    constructor() {
        this._navButtons = [];
        this.initNavButtons();
    }

    public get navButtons(): INavButtonModel[] {
        return this._navButtons;
    }

    private initNavButtons(): void {
        if (this._navButtons) {
            this._navButtons.push(
                {
                    displayName: "SNOWFLAKES",
                    imagePath: "../../../assets/snowflakes.png"
                }
            );
            this._navButtons.push(
                {
                    displayName: "MAGNETS",
                    imagePath: "../../../assets/magnets.png"
                }
            );
            this._navButtons.push(
                {
                    displayName: "LIQUIDS",
                    imagePath: "../../../assets/liquids.png",
                    subNavigation: [
                        "Flammable",
                        "Combustible",
                        "Flambustible"
                    ]
                }
            );
        }
    }
}
