import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/of";
import {INewsArticle} from "../models/INewsArticle";
import {INewsService} from "./INewsService";

/**
 * Mock data.
 */
@Injectable()
export class NewsService implements INewsService {

    private _newsArticles: INewsArticle[];

    constructor() {
        this._newsArticles = [];
        this.initFakeNews();
    }

    /**
     * Gets news by id.
     * @param {number} id The id of the article.
     * @returns {Observable<INewsArticle>} The article with the id.
     */
    public getNewsById(id: number): Observable<INewsArticle> {

        const newsArticle: INewsArticle = this._newsArticles.find(
            (newsArticle: INewsArticle) => {
                return newsArticle.id === id;
            }
        );

        return Observable.of(newsArticle);
    }

    private initFakeNews(): void {
        this._newsArticles.push({
            id: 1,
            author: "Bluehole",
            /* tslint:disable */
            body: `Players, as you know, we're always working towards optimizing our gameplay environment. 

 

We are planning to enhance the performance by adjusting some aspects of the game without impacting the gameplay in any conceivably negative way.

 

Through the last patch, we were able to improve server and client performance by adjusting the visible distance while the character is in the air. In the current patch we are spreading out the pre-match starting locations. Previously, all the players would spawn together at the same location awaiting the start of the match. Lots of interaction among multiple players in such a small area had a high impact on the servers. To solve this, we have introduced multiple areas where players gather before the match start. As a result, the performance, both server and client-side, has improved.

 

Additional modifications are also on the way related to the airplane performance improvement. More details will be shared soon.

 

We will also be testing a new security (anti-cheat) measure that is still under development but we want to start gathering some data regarding it's stability and compatibility. Some PCs may experience compatibility issues during this test, which may cause crashes when starting the game. If this happens to you, please take a screenshot of the error screen and post it here
 

When the changes on the test servers are evaluated to be stable, they will be applied to the live servers. The time for the live server update will be announced later.

 

Gameplay

 

Introduced multiple pre-match starting areas on both maps in order to optimize the server and game client performance
Removed weapons on starting island before the match start 
 

Language

 

Applied new Russian, Japanese and Chinese (simplified/traditional) fonts
 

Bug fixes

 

Fixed the issue where players were able to slide-jump from prone
 

 

 

Thank you.

The PUBG Development and Community Team`,
            /* tslint:enable */
            title: "PUBG PC 1.0 Update #3",
            time: new Date(2018, 0, 23, 17, 30, 30)
        });


        this._newsArticles.push({
            id: 2,
            author: "Bluehole",
            /* tslint:disable */
            body: `PC players,

 

After PC 1.0 launch, our highest priority has been ensuring a fair and competitive environment by combating cheats and decreasing the amount of their users as well as address any optimization and stability issues to create a more enjoyable environment. 

 

We recently discovered a new pattern of cheats in action. This week, we performed a thorough gameplay data review of 10 million players and completed analysis of tens of millions of data logs. Through this exercise, we were able to identify over 100,000 instances of the new pattern related to use of cheat and now we have confirmed that it was clearly an attempt of compromising our game. These players will be permanently banned in a single wave. This is an example of additional measures we will be taking on top of the basic detection systems in place. We will continue to check the data logs like this even if it means the anti-cheat team has to filter through hundreds of billions of data logs manually. In addition, we are looking into adopting a new solution to detect and ban more cheaters and we have been continuously strengthening our security systems. We have also liaised with investigative authorities in some countries to take legal actions against developers and distributors of cheats. We are determined to take strong measures against them going forward.

 

We have identified an issue in our deathcam, replay and observing systems which is related to showing other players as if they are using cheats. When you watch the gameplay video of another player in the systems, it looks like there is no recoil even for a player who is not using a cheat. Please note that it is caused by a bug in the systems. We apologize for the inconvenience this has caused in reporting cheaters and the confusion the bug has created for many of you. We are going to fix this bug as soon as possible. 

 

In addition to the high-priority issues, our development team has plans to enhance the combat experience in a match. The first plan is about rebalancing the blue zone. Over the course of the Early Access period, we received a lot of feedback from you that the pacing and distribution of combat encounters in a match could be more even and less chaotic. We performed a series of internal tests to find ways to address this issue. However, we were not able to take any action until now as other fundamental systems had to be developed and fixed first. Please note that we are still experimenting with the mechanics and we would like to hear your feedback. The changes you will see on the test servers are as follows:

 

Blue zone balance adjustment

 

Slightly decreased the waiting time of the blue zones in mid-to-late phase of a match 
Slightly decreased the shrinking speed of the blue zones in mid-to-late phase (In this phase, blue zones will now move at a slower speed and the travel time of blue zones has slightly increased due to this change) 
Slightly increased the damage per second of the last blue zone
 

 

These changes will be tested for several days. We will be testing this further after making changes to the above based on your feedback and play data. Please let us know your feedback on the adjusted blue zone mechanic compared with the current one on the live servers. 

 

We are planning to provide an update on other areas we have been working on. We will share the update as soon as possible. 

 

 

Thank you.

The PUBG Development and Community Team`,
            /* tslint:enable */
            title: "PUBG Testing Blue Zone Balance Adjustment & More",
            time: new Date(2018, 0, 19, 14, 30, 30)
        });

        this._newsArticles.push({
            id: 3,
            author: "Bluehole",
            /* tslint:disable */
            body: `Xbox players,



Quick update; we’ve released a server side patch this evening that addresses one of the lower frequency crashes as well as reduces intermittent in-game lag.

In addition we’ve made progress on the authentication issue that many players have encountered and we hope to completely resolve that issue soon.



We are still very focused on releasing a client side patch that incorporates some of the community feedback that has been raised, as well as priority bug fixes. We’ll have more to share on the timing of that patch in the upcoming days.

Please continue to visit https://forums.playbattlegrounds.com/forum/178-xbox/with any questions or comments.



See you in-game,

The PUBG Development and Community Team`,
            /* tslint:enable */
            title: "PUBG Xbox Game Preview Patch Notes #3",
            time: new Date(2017, 11, 28, 2, 30, 30)
        });

        this._newsArticles.push({
            id: 4,
            author: "Bluehole",
            /* tslint:disable */
            body: `Players,
Today, we are going to deploy a patch and the live servers will be under maintenance for two hours.

Before we dive into the patch notes, we would like to sincerely apologize for the inconvenience caused by the intermittent lag and character position readjustment issues our players are experiencing during matches. Creating a smooth playing environment to allow our players to fully enjoy Battle Royale was one of our top priorities for PC 1.0 launch. However, a multitude of causes have triggered the in-game lag issue to become worse or happen more frequently in the last few weeks. In the process, we were not able to implement the measures to visibly address the issue more quickly.

Contrary to most expectations, there is a combination of causes that lead to the in-game lag and character position readjustment issues. The issues cannot be attributed to one or two causes and have stemmed from a combination of different factors which is why we need to continuously work on resolving and investigating the problem. We upgraded our game engine to the latest version and made changes to the game when we added new content. The problem resulted from these changes as well as several other causes.

In this patch, we have removed some inefficiencies in server infrastructure and optimized in-game servers to alleviate the problem. More specifically, we made adjustments to reduce the bottleneck during the game server launch phase and also resolved some server hitch issues.

Resolving the intermittent lag and character position readjustment issues are still one of our top priorities, and we are continuing to work on analyzing and fixing any remaining issues every day. After today’s update, we will be running some internal tests and deploying more updates to gradually mitigate the problem. We are currently examining several measures including server optimization and server logic modification to address the multitude of causes. On top of this, we will continue our efforts to further investigate remaining causes.

We will keep you updated on our progress going forward. We would like to apologize again for the inconvenience and we will continue to do our best to resolve the issues.

Please find the notes for the today’s patch below.



World
Changed the map selection ratio to be equal for Erangel and Miramar.

Replays
Adjusted replay system so that players can only view game replays from 3 minutes after death.
This is to prevent sharing opponent’s positions to surviving teammates immediately after death.

Bug Fixes
Fixed the issue where some buildings and objects were not marked on the Miramar world map.
Reduced occurrences of the issue where replays occasionally crashed back to the lobby screen.
On restarting a replay, it will continue where last viewed.
Fixed the issue where the beginning of the Death Cam and replay was missing.
Fixed the issue where players couldn’t earn BP.
We will update with a compensation plan soon.
Fixed the issue where players could die unexpectedly from fall damage.
Fixed the issue where bullets couldn’t penetrate particular fences from both sides.

Thank you.
​The PUBG Development and Community Team`,
            /* tslint:enable */
            title: "PUBG PC 1.0 Update #1",
            time: new Date(2017, 11, 37, 9, 30, 30)
        });

        this._newsArticles.push({
            id: 5,
            author: "Bluehole",
            /* tslint:disable */
            body: `Xbox players,



Sincere thanks for your continued feedback, and as usual please direct your thoughts and issues to: https://forums.playbattlegrounds.com/forum/178-xbox/



The team will continue to evaluate areas for improvement, whether its client and server optimizations or monitoring the current “Car Meta”. Our second patch is focused on quality of life fixes (weapon recoil for example) as well as trying to resolve some of the stability issues that have been brought to our attention.



We expect to release our next patch at the beginning of the year, but we will monitor player feedback carefully and will address critical items over the holiday break as needed.

Thanks again for your support.



This patch will be live at 1AM PST / 10AM CET / 6PM KST.

Please restart your Xbox after the patch goes live to get the update.



Optimization

Continued performance optimizations



In-game

The tuning range for the general sensitivity option has been expanded

Weapon recoil reduced across the board (Xbox Only)

Team UI will now correctly display voice chat status

Additional Localization updates



Bug

A variety of crash fixes

Fixed an issue where casting bar UI remains onscreen when spectating a target who died while using an item from their inventory



See you in-game,

The PUBG Development and Community Team`,
            /* tslint:enable */
            title: "PUBG Xbox Game Preview Patch Notes #2",
            time: new Date(2017, 11, 21, 22, 30, 30)
        });
    }
}
