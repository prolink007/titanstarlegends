import {Injectable} from "@angular/core";
import {ITalentCommsService} from "./ITalentCommsService";
import {Subject} from "rxjs/Subject";
import {ITalentNode} from "../models/ITalentNode";
import {Observable} from "rxjs/Observable";

/**
 * A service for component comms.
 */
@Injectable()
export class TalentCommsService implements ITalentCommsService {

    private _talentSelectedSource: Subject<ITalentNode>;

    constructor() {
        this._talentSelectedSource = new Subject<ITalentNode>();
    }

    public talentSelected(): Observable<ITalentNode> {
        return this._talentSelectedSource.asObservable();
    }

    public selectTalent(talentNode: ITalentNode): void {
        this._talentSelectedSource.next(talentNode);
    }
}
