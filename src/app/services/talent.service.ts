import {Injectable} from "@angular/core";
import {ITalentTree} from "../models/ITalentTree";
import {ITalentService} from "./ITalentService";
import {ITalentPath} from "../models/ITalentPath";
import {TSLTalentPath} from "../models/TSLTalentPath";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/of";

/**
 * Mock data.
 */
@Injectable()
export class TalentService implements ITalentService {

    public getRuneMasteryTalentTree(): Observable<ITalentTree> {
        return Observable.of(this.createTalentTree());
    }

    private createTalentTree(): ITalentTree {
        return {
            talentPaths: [
                this.createTalentPath1(),
                this.createTalentPath2()
            ],
            name: "Rune Mastery",
            totalPointsAvailable: 6
        };
    }

    private createTalentPath1(): ITalentPath {
        const talentPath: TSLTalentPath = new TSLTalentPath();

        talentPath.name = "TALENT PATH 1";

        talentPath.addTalentNode(
            {
                name: "Cube",
                fullIcon: "../../assets/cube_lite.png",
                notFullIcon: "../../assets/cube_dark.png",
                pointMax: 1,
                selected: false,
                valid: true
            }
        );

        talentPath.addTalentNode(
            {
                name: "Food Weapons",
                fullIcon: "../../assets/food_weapons_lite.png",
                notFullIcon: "../../assets/food_weapons_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        talentPath.addTalentNode(
            {
                name: "Cake",
                fullIcon: "../../assets/cake_lite.png",
                notFullIcon: "../../assets/cake_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        talentPath.addTalentNode(
            {
                name: "Crown",
                fullIcon: "../../assets/crown_lite.png",
                notFullIcon: "../../assets/crown_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        return talentPath;
    }

    private createTalentPath2(): ITalentPath {
        const talentPath: TSLTalentPath = new TSLTalentPath();

        talentPath.name = "TALENT PATH 2";

        talentPath.addTalentNode(
            {
                name: "Boat",
                    fullIcon: "../../assets/boat_lite.png",
                notFullIcon: "../../assets/boat_dark.png",
                pointMax: 1,
                selected: false,
                valid: true
            }
        );

        talentPath.addTalentNode(
            {
                name: "Scuba",
                    fullIcon: "../../assets/scuba_lite.png",
                notFullIcon: "../../assets/scuba_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        talentPath.addTalentNode(
            {
                name: "Lightning Bolt",
                    fullIcon: "../../assets/lightning_bolt_lite.png",
                notFullIcon: "../../assets/lightning_bolt_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        talentPath.addTalentNode(
            {
                name: "Skull",
                    fullIcon: "../../assets/skull_lite.png",
                notFullIcon: "../../assets/skull_dark.png",
                pointMax: 1,
                selected: false,
                valid: false
            }
        );

        return talentPath;
    }
}
